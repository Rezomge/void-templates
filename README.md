# void-templates

This is a repository of my templates for Void Linux.

## Usage

### Automatic method
1. Create work directory
```
mkdir ~/git
```
2. Clone this repository
```
git clone https://codeberg.org/Rezomge/void-templates && cd void-templates/scripts
```

3. Run srcpkgs-installer.sh
```
chmod +x srcpkgs-installer.sh
./srcpkgs-installer.sh
```

4. Run package building script
```
cd packages
chmod +x build-package.sh
./build-package.sh
```

### Manual method
1. You may want to start by making a directory where you can keep the relevant repositories
```
mkdir ~/git
cd ~/git
```
2. Clone void-packages repo
```
git clone https://github.com/void-linux/void-packages
cd void-packages
./xbps-src binary-bootstrap
cd ..
```
3. Clone this repository:
```
git clone https://codeberg.org/Rezomge/void-templates && cd void-templates
```
4. Copy srcpkgs to your void-packages srcpkgs directory
```
cp -r srcpkgs/* ../void-packages/srcpkgs
```
5. Build and install packages
```
cd ../void-packages
./xbps-src pkg hyprpicker
sudo xbps-install -R hostdir/binpkgs hyprpicker
```

## Updating

If packages updates and this repository changes, you may want to perform a hard reset and clean of your cloned void-packages repository to ensure changes are correctly applied when repeating steps 4 and 5 after a git pull on both void-packages and void-templates. (BEWARE: This will also reset any changes you have made to any other packages locally - you will have to figure it out yourself in this case)

### Automatic method
```
1. Run repo-updater.sh
cd scripts
chmod +x repo-updater.sh
./repo-updater.sh
```

### Manual method
```
sudo xbps-install -Su # Update system normally first to avoid building every package needing update from source

cd ~/git/void-packages
git clean -fd
git reset --hard
git pull

cd ../void-templates
git pull

cp -r srcpkgs/* ../void-packages/srcpkgs

cd ../void-packages
./xbps-src update-sys
```

## Note on Updating

Sometimes old stuff isn't particularly easy to clean up between versions, and it may be necessary to remove your old void-packages clone and just reclone it. If you run into problems when updating, please try this first before reporting the issue.

```
# remove old clone of void-packages
cd ~/git
rm -r void-packages

# make a fresh clone of void-packages
git clone https://github.com/void-linux/void-packages
cd void-packages
./xbps-src binary-bootstrap

# update void-packages and copy hyprland-void stuff again
cd ../void-packages
git pull
cp -r srcpkgs/* ../void-packages/srcpkgs

# update
cd ../void-packages
sudo xbps-install -Su
./xbps-src update-sys
```