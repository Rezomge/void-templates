#!/bin/bash

if [ -d $HOME/git/void-packages ]
then
	cd ~/git/void-packages
	git clean -fd
	git reset --hard
	git pull
else
	cd ~/git
	git clone https://github.com/void-linux/void-packages.git
	cd ~/git/void-packages
	./xbps-src binary-bootstrap
	echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf
fi
