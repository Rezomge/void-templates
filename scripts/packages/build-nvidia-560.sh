#!/bin/bash

PACKAGE=nvidia

if [ -d $HOME/git/void-packages ]
then
	cd $HOME/git/void-packages
	cp $HOME/git/void-templates/srcpkgs/nvidia-560/template $HOME/git/void-packages/srcpkgs/$PACKAGE
	./xbps-src pkg $PACKAGE
	sudo xbps-install -f --repository hostdir/binpkgs/nonfree nvidia nvidia-dkms nvidia-firmware nvidia-gtklibs nvidia-opencl nvidia-libs
	sudo xbps-install -f --repository hostdir/binpkgs/multilib/nonfree/ nvidia-libs-32bit
else
	echo "Error void-packages repo not found"
fi

