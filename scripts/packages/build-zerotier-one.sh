#!/bin/bash

PACKAGE=zerotier-one

if [ -d $HOME/git/void-packages ]
then
	cd $HOME/git/void-packages
	if [ -d $HOME/git/void-packages/srcpkgs/$PACKAGE ]
	then
		rm -r $HOME/git/void-packages/srcpkgs/$PACKAGE
		cp -r $HOME/git/void-templates/srcpkgs/$PACKAGE $HOME/git/void-packages/srcpkgs/$PACKAGE
		./xbps-src pkg $PACKAGE
		sudo xbps-install --repository hostdir/binpkgs/nonfree $PACKAGE
	else
		cp -r $HOME/git/void-templates/srcpkgs/$PACKAGE $HOME/git/void-packages/srcpkgs/$PACKAGE
		./xbps-src pkg $PACKAGE
		sudo xbps-install --repository hostdir/binpkgs/nonfree $PACKAGE
	fi
else
	echo "Error void-packages repo not found"
fi
